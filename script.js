/* DOKUWIKI:include_once lightGallery/js/lightgallery.min.js */
/* DOKUWIKI:include_once lightGallery/js/picturefill.min.js */
/* DOKUWIKI:include_once lightGallery/js/lg-fullscreen.min.js */
/* DOKUWIKI:include_once lightGallery/js/lg-thumbnail.min.js */
/* DOKUWIKI:include_once lightGallery/js/lg-video.min.js */
/* DOKUWIKI:include_once lightGallery/js/lg-autoplay.min.js */
/* DOKUWIKI:include_once lightGallery/js/lg-zoom.min.js */
/* !DOKUWIKI:include_once lightGallery/js/lg-hash.min.js */
/* !DOKUWIKI:include_once lightGallery/js/lg-pager.min.js */
/* DOKUWIKI:include_once lightGallery/js/jquery.mousewheel.min.js */
		
/**
 * Initialize lightGallery for pg-show class
 */
function InitPgGallery(elt,tw,th,play){
		elt.lightGallery({
				thumbnail:true,
				autoplay:play,
				showAfterLoad:true,
				pause:4000,
				preload:1,
				mode:"lg-fade",
				thumbWidth:tw,
				thumbContHeight:th
		});
    return false;
}

/**
 * Attach click event to the poster <a> tag
   Runs when the DOM is ready
 */
jQuery(document).on('click', 'a.pg-start', function() {
	var $pgid = '#' + jQuery(this).data('pg-id');
	var $pg = jQuery($pgid);
    if($pg.children().length == 0) {
      var encoded = $pg.html();
      $pg.html(decodeURIComponent((encoded+'').replace(/\+/g, '%20')));
    }
    eval(jQuery(this).data('script'));
	var $img = $pg.children('li').children('img');
	$img.trigger("click");
	return false;
});
